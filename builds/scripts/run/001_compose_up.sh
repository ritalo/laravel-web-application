#!/usr/bin/env bash

"${COMPOSE_CMD[@]}" up -d app nginx mysql localstack redis

if [[ $CI_MODE == true ]]; then
    APP_ID=$(get_container_id app)
    docker cp . "${APP_ID}:/cnyes_code"
    "${COMPOSE_EXEC_CMD[@]}" app chown -R www-data.www-data storage
fi
